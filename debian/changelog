zeek (3.2.3+ds2-2) unstable; urgency=medium

  * Fix FTBFS on armel

 -- Hilko Bengen <bengen@debian.org>  Mon, 08 Feb 2021 22:33:03 +0100

zeek (3.2.3+ds2-1) unstable; urgency=medium

  * Add vendored highwayhash, libkqueue sources

 -- Hilko Bengen <bengen@debian.org>  Mon, 08 Feb 2021 18:31:24 +0100

zeek (3.2.3+ds1-2) unstable; urgency=medium

  * Source-only re-upload

 -- Hilko Bengen <bengen@debian.org>  Thu, 31 Dec 2020 13:18:44 +0100

zeek (3.2.3+ds1-1) unstable; urgency=medium

  * New upstream version 3.2.3+ds1
  * Add vendored rapidjson git snapshot (v1.1.0-569-g56f215e5, 2019-12-23)
  * Update patches, build-dependencies, build script

 -- Hilko Bengen <bengen@debian.org>  Thu, 31 Dec 2020 13:18:35 +0100

zeek (3.0.8+ds1-1) experimental; urgency=medium

  * New upstream version 3.0.8+ds1
  * Closes: #965976

 -- Hilko Bengen <bengen@debian.org>  Tue, 04 Aug 2020 09:27:26 +0200

zeek (3.0.7+ds1-2) experimental; urgency=medium

  * Fix indentation in debian/copyright
  * Add missing misc:Depends for bro transitional package

 -- Hilko Bengen <bengen@debian.org>  Fri, 19 Jun 2020 19:29:28 +0200

zeek (3.0.7+ds1-1) experimental; urgency=medium

  * New upstream version 3.0.7+ds1
  * Add git-buildpackage configuration

 -- Hilko Bengen <bengen@debian.org>  Thu, 11 Jun 2020 15:01:11 +0200

zeek (3.0.6+ds1-1) experimental; urgency=medium

  * New upstream version 3.0.6+ds1
  * Rename package bro -> zeek, update packaging.
  * Update watch file
  * Consolidate build-system-related patches

 -- Hilko Bengen <bengen@debian.org>  Sun, 31 May 2020 17:49:59 +0200

bro (2.6.4+ds1-2) unstable; urgency=medium

  * Drop libbind-dev build-dependency (Closes: #942498)

 -- Hilko Bengen <bengen@debian.org>  Thu, 17 Oct 2019 14:02:39 +0200

bro (2.6.4+ds1-1) unstable; urgency=medium

  * Remove Raúl Benencia from Uploaders, per his request
  * New upstream version 2.6.4+ds1
  * Restore GeoIP support: Build against libmaxminddb which replaces
    libgeoip (Closes: #936038)
  * Bump binpac build-dependency

 -- Hilko Bengen <bengen@debian.org>  Tue, 03 Sep 2019 23:27:02 +0200

bro (2.6.1+ds1-1) unstable; urgency=medium

  * New upstream version 2.6.1+ds1
  * Add instructions to repack orig tarball: remove 3rdparty sources
  * Drop unneeded dh --parallel parameter
  * Bump Debhelper compat level
  * Replace dh_install --fail-missing
  * Bump Standards-Version
  * Rebase patches, drop OpenSSL-1.1-related changes
  * Add build-dependencies
  * Fix homepage, Vcs-Git URL
  * Add bro-dev package
  * Avoid copying extra license file
  * Add Lintian override for source package

 -- Hilko Bengen <bengen@debian.org>  Thu, 07 Feb 2019 16:23:56 +0100

bro (2.5.5-1) unstable; urgency=medium

  * New upstream version 2.5.5
  * Bump Standards-Version
  * Bump binpac build-dependency
  * copyright: Add bits of Creative Commons license that are not strictly
    part of the license (thanks, Lintian)
  * Make build reproducible by removeing build path from *.bro files,
    thanks to Chris Lamb (Closes: #908379)

 -- Hilko Bengen <bengen@debian.org>  Wed, 05 Sep 2018 16:05:40 +0200

bro (2.5.4-1) unstable; urgency=medium

  * New upstream version 2.5.4
  * Bump binpac build-dependency
  * Bump Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Sun, 17 Jun 2018 12:47:27 +0200

bro (2.5.3-1) unstable; urgency=medium

  * New upstream version 2.5.3
  * Bump Standards-Version
  * Update VCS fields for salsa
  * Bump version in binpac build-dependency
  * Clean up debian/rules

 -- Hilko Bengen <bengen@debian.org>  Thu, 15 Feb 2018 17:08:02 +0100

bro (2.5.2-2) unstable; urgency=medium

  * Add patches to port everything but OCSP-related features to OpenSSL
    1.1 (Closes: #851091)

 -- Hilko Bengen <bengen@debian.org>  Wed, 29 Nov 2017 16:40:09 +0100

bro (2.5.2-1) unstable; urgency=medium

  * Update upstream's public key
  * New upstream version 2.5.2
  * Modernize package: Bump DH compat level, Standards-Version; 
    fix VCS-* URLs

 -- Hilko Bengen <bengen@debian.org>  Tue, 17 Oct 2017 10:54:57 +0200

bro (2.5.1-1) unstable; urgency=medium

  * New upstream version 2.5.1

 -- Hilko Bengen <bengen@debian.org>  Sun, 16 Jul 2017 14:23:59 +0200

bro (2.5-1) unstable; urgency=medium

  * New upstream version 2.5
  * Update watch file
  * Update patches
  * No longer strip documentation from tarball -- it has a DFSG-free
    license now.
  * Bump Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Fri, 23 Dec 2016 23:36:01 +0100

bro (2.4.1+dfsg-4) unstable; urgency=medium

  * Disable OpenSSL 1.1 patches, build using libssl1.0-dev instead

 -- Hilko Bengen <bengen@debian.org>  Tue, 22 Nov 2016 09:08:22 +0100

bro (2.4.1+dfsg-3) unstable; urgency=medium

  * Port most of the OpenSSL-using code to OpenSSL 1.1 (Closes: #828254).
    OCSP validation stays disabled for now. Thanks to Sascha Steinbiss for
    his help.

 -- Hilko Bengen <bengen@debian.org>  Tue, 15 Nov 2016 12:35:28 +0100

bro (2.4.1+dfsg-2) unstable; urgency=medium

  * Fix FTBFS with OpenSSL built without SSLv3 support

 -- Hilko Bengen <bengen@debian.org>  Thu, 05 Nov 2015 18:24:43 +0100

bro (2.4.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Hilko Bengen <bengen@debian.org>  Thu, 29 Oct 2015 19:01:12 +0100

bro (2.4+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Rebased patch queue
  * Deleted our manpage since there is now one in the upstream sources

 -- Hilko Bengen <bengen@debian.org>  Sun, 28 Jun 2015 16:37:10 +0200

bro (2.3.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add missing entry to debian/copyright
  * Drop bro-doc package. The documentation can't be packaged in main
    because of license issues (CC BY-NC 3.0)
  * Add script for stripping the doc/ directory from the original tarball
  * Adjust binpac build-dependency

 -- Hilko Bengen <bengen@debian.org>  Tue, 03 Feb 2015 21:14:50 +0100

bro (2.3.1-2) unstable; urgency=medium

  * Fix FTBFS when building architecture-dependent package only

 -- Hilko Bengen <bengen@debian.org>  Sat, 13 Dec 2014 15:47:12 +0100

bro (2.3.1-1) unstable; urgency=low

  * Initial release (Closes: #752546)

 -- Hilko Bengen <bengen@debian.org>  Fri, 05 Dec 2014 22:14:18 +0100
